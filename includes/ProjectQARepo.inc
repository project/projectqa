</<?php

/**
 * ProjectQARepo class.
 */
class ProjectQARepo extends Entity {

  /**
   * The default label.
   *
   * @return int
   *   projectqa_repo id
   */
  protected function defaultLabel() {
    return $this->prid;
  }

  /**
   * The default URI.
   *
   * @return array
   *   An array containing the path to the projectqa_repo
   */
  protected function defaultUri() {
    return array('path' => 'projectqa_repo/' . $this->identifier());
  }
}


/**
 * ProjectQARepoController class
 */
class ProjectQARepoController extends EntityAPIController {}

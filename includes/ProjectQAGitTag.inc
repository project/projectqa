</<?php

/**
 * ProjectQAGitTag class.
 */
class ProjectQAGitTag extends Entity {

  /**
   * The default label.
   *
   * @return int
   *   projectqa_gittag id
   */
  protected function defaultLabel() {
    return $this->gcid;
  }

  /**
   * The default URI.
   *
   * @return array
   *   An array containing the path to the projectqa_gittag
   */
  protected function defaultUri() {
    return array('path' => 'projectqa_gittag/' . $this->identifier());
  }
}


/**
 * ProjectQAProjectController class
 */
class ProjectQAGitTagController extends EntityAPIController {}

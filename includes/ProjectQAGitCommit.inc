<?php

/**
 * ProjectQAGitCommit class.
 */
class ProjectQAGitCommit extends Entity {
  /**
   * The default label.
   *
   * @return int
   *   projectqa_gitcommit id
   */
  protected function defaultLabel() {
    return $this->gcid;
  }

  /**
   * The default URI.
   *
   * @return array
   *   An array containing the path to the projectqa_gitcommit
   */
  protected function defaultUri() {
    return array('path' => 'projectqa_gitcommit/' . $this->identifier());
  }
}


/**
 * ProjectQAProjectController class
 */
class ProjectQAGitCommitController extends EntityAPIController {}
